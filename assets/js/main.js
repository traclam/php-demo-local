const Demo = {
    init: function() {
        console.log( $( 'body' ) );
    }
};

// Include jQuery library
var script = document.createElement('script');
script.src = 'https://code.jquery.com/jquery-3.6.0.min.js';
script.onload = function() {
    // jQuery is loaded, you can now use it as $
    $(document).ready(function() {
        Demo.init();
    });
};
document.head.appendChild(script);
<!DOCTYPE html>
<html>
<head>
    <title>Hello, World!</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <script src="/assets/js/main.js"></script>
</head>
<body>
    <div class="container">

        <h1 class="site-heading">HTTP/2 Protocol</h1>

        <section class="site-section">
            <h2>Lịch sử HTTP</h2>
            <div class="row">
                <div class="col-2">
                    <ul>
                        <li>HTTP (HyperText Transfer Protocol) là một giao thức dùng để truyền tải dữ liệu qua mạng Internet.</li>
                        <li>HTTP được phát triển bởi Tim Berners-Lee tại CERN vào năm 1989.</li>
                    </ul>
                    <p><img src="https://statics.cdn.200lab.io/2023/05/http2-la-gi.png"></p>
                    <ul>
                        <li>HTTP v0.9 chỉ support mỗi một method duy nhất là GET.</li>
                        <li>Kiểu trả về duy nhất là Hypertext và không có Header.</li>
                    </ul>
                    <p><img src="https://statics.cdn.200lab.io/2023/05/http2-la-gi.jpeg"></p>
                </div>
                <div class="col-2">
                    <img src="https://i.insider.com/5b3b49557708e975933334be?width=700" alt="Tim Berners-Lee">
                </div>
            </div>
        </section>

        <section class="site-section">
            <h2>HTTP/1 (v1.1)</h2>
            <div class="row">
                <div class="col-2">
                    <ul>
                        <li>HTTP/1.1 là phiên bản được sử dụng phổ biến nhất hiện nay.</li>
                        <li>Hỗ trợ nhiều kiểu trả về như HTML, JSON, XML, ...</li>
                        <li>Hỗ trợ nhiều Header như Accept, Accept-Charset, Accept-Encoding, Accept-Language, Authorization, Cache-Control, Connection, Content-Length, Content-Type, Cookie, Host, User-Agent, ...</li>
                        <li>Hỗ trợ nhiều method như GET, POST, PUT, DELETE, OPTIONS, HEAD, PATCH, CONNECT.</li>
                        <li>Quan trọng nhất là TCP Connection có thể được giữ lại (keep-alive hay persistent connection) để phục vụ các request tiếp theo.</li>
                        <li>Hỗ trợ Compression/Decompression (Encoding).</li>
                    </ul>
                    <h3>HTTP Pipelining</h3>
                    <ul>
                        <li>HTTP Pipelining là một kỹ thuật giúp tăng tốc độ truyền tải dữ liệu giữa Client và Server.</li>
                        <li>Cho phép Client gửi nhiều request đến Server mà không cần phải chờ Server trả về response.</li>
                        <li>Giúp giảm thiểu độ trễ giữa các request và response.</li>
                        <li>Quy tắc là request thứ tự ra sao thì phải trả về đúng theo thứ tự đó.</li>
                    </ul>
                </div>
                <div class="col-2">
                    <img src="https://statics.cdn.200lab.io/2023/05/http2-la-gi-4.png">
                </div>
            </div>
        </section>

        <section class="site-section">
            <h2>SPDY</h2>
            <div class="row">
                <div class="col-2">
                    <ul>
                        <li>SPDY (đọc là “speedy”) được phát triển bởi Google bắt đầu từ năm 2009 (v1) và 2012 (v2)</li>
                        <li>Mục đích giao thức SPDY ra đời để tăng tốc độ tải trang và tính bảo mật cho website.</li>
                    </ul>
                    <h3>Một số vấn đề từ HTTP/1 mà SPDY ra đời để giải quyết</h3>
                    <ul>
                        <li>Single request per connection:<br>HTTP chỉ có thể tải về từng resource với từng request, mỗi cái là một connection tương ứng. Việc này gây làm tăng delay (thời gian chờ) cũng như không tận dụng được băng thông. SPDY cho phép một connection có thể xử lý được đồng thời nhiều request.</li>
                        <li>Client-initiated requests:<br>HTTP hoạt động theo kiểu “hỏi-đáp”, client request dữ liệu nào thì được server trả về dữ liệu nấy. Với SPDY, server có thể chủ động “push” dữ liệu về client mà không cần client gởi request đến.</li>
                        <li>Redundant headers:<br>HTTP headers là những metadata mô tả cách thức vận hành của một request HTTP. Trong nhiều trường hợp, rất nhiều request với những header lập đi lập lại giống nhau. SPDY có thể loại bỏ những header không cần thiết này để giải lượng băng thông cần thiết.</li>
                        <li>Uncompressed data:<br>Compression (nén) sẽ giúp dữ liệu có dung lượng nhỏ hơn khi trao đổi thông tin, với HTTP thì nó là optional (không bắt buộc). SPDY bắt buộc sử dụng nén dữ liệu cho mọi request.</li>
                    </ul>
                </div>
                <div class="col-2">
                    <img src="https://statics.cdn.200lab.io/2023/05/http2-la-gi-7.png">
                </div>
            </div>
        </section>

        <section class="site-section">
            <h2>HTTP/2 (v2.0)</h2>
            <div class="row">
                <div class="col-2">
                    <ul>
                        <li>HTTP/2 là phiên bản mới nhất của giao thức HTTP dựa trên công nghệ lõi từ SPDY được phát triển bởi Google.</li>
                        <li>Được phát triển bởi IETF (Internet Engineering Task Force).</li>
                        <li>Được thiết kế để giảm thiểu độ trễ, tăng tốc độ truyền tải dữ liệu giữa Client và Server.</li>
                        <li>Sử dụng kỹ thuật Multiplexing giúp gửi nhiều request đến Server mà không cần phải chờ Server trả về response.</li>
                        <li>Sử dụng kỹ thuật Header Compression giúp giảm dung lượng Header gửi đi.</li>
                        <li>Sử dụng kỹ thuật Server Push giúp Server gửi dữ liệu đến Client mà không cần phải Client yêu cầu.</li>
                        <li><a href="https://caniuse.com/http2" target="_blank">Browser support</a></li>
                    </ul>
                    <h3>Tính năng nổi bật</h3>
                    <ul>
                        <li>HTTP/2 sử dụng Binary Protocol thay cho dạng văn bản</li>
                        <li>Request Response Multiplexing: định danh để biết được response nào của request nào.</li>
                        <li>Streams: Kết nối HTTP/2 sẽ bao gồm nhiều Stream (luồng dữ liệu). Các streams này chứa một dãy các Data Frame cần giao tiếp giữa client và server. Các Data Frame có thể được đặt xen kẽ bất kể chúng từ đâu đến.</li>
                        <li>Stateful Header Compression: Trong lúc client thực hiện các request đến server sẽ có vô số các header bị dư thừa và lặp đi lặp lại. HTTP/2 sử dụng HPACK như một cách tiếp cập đơn giản và an toàn để compress (nén) các header này.</li>
                        <li>Server Push: Trong HTTP/2 server có thể gởi về nhiều response với chỉ một request từ client. Cơ chế này gọi là Server Push, giúp trình duyệt tiết kiệm được các requests không cần thiết.</li>
                    </ul>
                </div>
                <div class="col-2">
                    <p><img src="https://statics.cdn.200lab.io/2023/05/http2-la-gi-8.png" /></p>
                    <p><img src="https://statics.cdn.200lab.io/2023/05/http2-la-gi-12.png" /></p>
                    <p><img src="https://statics.cdn.200lab.io/2023/05/http2-la-gi-13.png" /></p>
                </div>
            </div>
        </section>

        <section class="site-section">
            <h2>HTTP/2 vs HTTP/1 Demo</h2>
            <iframe class="embed-iframe" src="http://www.http2demo.io/"></iframe>
        </section>

    </div>
</body>
</html>